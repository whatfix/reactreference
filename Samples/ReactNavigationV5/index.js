/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './src/App';

AppRegistry.registerComponent("MyApp5", () => App);
