import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';

import SettingsScreen from '../components/SettingsScreen';
import ListScreen from '../components/ListScreen';
const Drawer = createDrawerNavigator();

export default function MDrawerNavigator() {
    return (
            <Drawer.Navigator initialRouteName="Settings">
                <Drawer.Screen name="Settings" component={SettingsScreen} />
                <Drawer.Screen name="List" component={ListScreen} />
            </Drawer.Navigator>
    );
}

