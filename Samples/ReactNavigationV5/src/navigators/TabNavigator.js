import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import HomeScreen from '../components/HomeScreen';
import AboutScreen from '../components/AboutScreen';

const Tab = createBottomTabNavigator();

export default function MTabNavigator() {
  return (
        <Tab.Navigator>
          <Tab.Screen name="Home" component={HomeScreen} />
          <Tab.Screen name="About" component={AboutScreen} />
        </Tab.Navigator>
  );
}
