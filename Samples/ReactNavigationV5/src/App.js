import React, { useEffect } from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import RNWhatfixModule from 'rn-whatfix';

import HomeScreen from './components/HomeScreen';
import AboutScreen from './components/AboutScreen';

const Stack = createStackNavigator();

function App() {
  const routeNameRef = React.useRef();
  const navigationRef = React.useRef();

  useEffect(() => {
    RNWhatfixModule.initialize('');
    RNWhatfixModule.setScreenId(navigationRef.current.getCurrentRoute().name);
  }, []);

  return (
    <NavigationContainer
      ref={navigationRef}
      onReady={() =>
        (routeNameRef.current = navigationRef.current.getCurrentRoute().name)
      }
      onStateChange={() => {
        const previousRouteName = routeNameRef.current;
        const currentRouteName = navigationRef.current.getCurrentRoute().name;
        if (previousRouteName !== currentRouteName) {
          RNWhatfixModule.setScreenId(currentRouteName);
        }
        routeNameRef.current = currentRouteName;
      }}>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="About" component={AboutScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
