import React, { useEffect } from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import RNWhatfixModule from 'rn-whatfix';

import MTabNavigator from './navigators/TabNavigator';
import MDrawerNavigator from './navigators/DrawerNavigator';

const Stack = createStackNavigator();

function App() {
    const routeNameRef = React.useRef();
    const navigationRef = React.useRef();

    useEffect(() => {
        RNWhatfixModule.initialize('');
        RNWhatfixModule.setScreenId(navigationRef.current.getCurrentRoute().name);
    }, []);

    return (
        <NavigationContainer
            ref={navigationRef}
            onReady={() =>
                (routeNameRef.current = navigationRef.current.getCurrentRoute().name)
            }
            onStateChange={() => {
                const previousRouteName = routeNameRef.current;
                const currentRouteName = navigationRef.current.getCurrentRoute().name;
                if (previousRouteName !== currentRouteName) {
                    RNWhatfixModule.setScreenId(currentRouteName);
                }
                routeNameRef.current = currentRouteName;
            }}>
            <Stack.Navigator>
                <Stack.Screen name="Tab" component={MTabNavigator} />
                <Stack.Screen name="Drawer" component={MDrawerNavigator} />
            </Stack.Navigator>
        </NavigationContainer>
    );
}

export default App;
