[For more details](https://reactnavigation.org/docs/getting-started)

```jsx
import React, { useEffect } from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import RNWhatfixModule from 'rn-whatfix';

const Stack = createStackNavigator();

function App() {
  const routeNameRef = React.useRef();
  const navigationRef = React.useRef();

  useEffect(() => {
    RNWhatfixModule.initialize('<ent_id>');
    RNWhatfixModule.setScreenId(navigationRef.current.getCurrentRoute().name);
  }, []);

  return (
    <NavigationContainer
      ref={navigationRef}
      onReady={() =>
        (routeNameRef.current = navigationRef.current.getCurrentRoute().name)
      }
      onStateChange={() => {
        const previousRouteName = routeNameRef.current;
        const currentRouteName = navigationRef.current.getCurrentRoute().name;
        if (previousRouteName !== currentRouteName) {
          RNWhatfixModule.setScreenId(currentRouteName);
        }
        routeNameRef.current = currentRouteName;
      }}>
      <Stack.Navigator>
        <Stack.Screen name="Screen1" component={Screen1} />
        <Stack.Screen name="Screen2" component={Screen2} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
```
