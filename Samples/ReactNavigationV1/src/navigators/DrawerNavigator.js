import { DrawerNavigator } from 'react-navigation';

import Screen3 from '../components/Screen3';
import Screen4 from '../components/Screen4';

const MDrawerNavigator = new DrawerNavigator(
    {
        Screen3: Screen3,
        Screen4: Screen4,
    },
    {
      drawerBackgroundColor: 'rgba(255,255,255,.9)',
      contentOptions: {
        activeTintColor: '#fff',
        activeBackgroundColor: '#6b52ae',
      },
    }
);

export default MDrawerNavigator;

