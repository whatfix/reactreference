import { TabNavigator } from 'react-navigation';

import HomeScreen from '../components/HomeScreen';
import AboutScreen from '../components/AboutScreen';

const MTabNavigator=new TabNavigator({
  HomeScreen: { screen: HomeScreen },
  AboutScreen: { screen: AboutScreen },
});
export default MTabNavigator;
