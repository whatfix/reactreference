import React from 'react';
import { StackNavigator } from 'react-navigation';
import RNWhatfixModule from 'rn-whatfix';

import HomeScreen from './components/HomeScreen';
import AboutScreen from './components/AboutScreen';

const AppNavigator = StackNavigator({
    Home: { screen: HomeScreen },
    About: { screen: AboutScreen },
    }
);

function getCurrentRouteName(navigationState) {
  if (!navigationState) {
    return null;
  }
  const route = navigationState.routes[navigationState.index];
  if (route.routes) {
    return getCurrentRouteName(route);
  }
  return route.routeName;
}

export default class App extends React.Component {

  componentDidMount() {
    RNWhatfixModule.initialize('');
    RNWhatfixModule.setScreenId('Home');
  }
  render() {
    return (
      <AppNavigator
        onNavigationStateChange={(prevState, currentState, action) => {
          const currentRouteName = getCurrentRouteName(currentState);
          const previousRouteName = getCurrentRouteName(prevState);

          if (previousRouteName !== currentRouteName) {
            RNWhatfixModule.setScreenId(currentRouteName);
          }
        }}
      />
    );
  }
}
