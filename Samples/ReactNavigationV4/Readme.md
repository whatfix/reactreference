[For more details](https://reactnavigation.org/docs/4.x/getting-started)

```jsx
import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import RNWhatfixModule from 'rn-whatfix';

const AppNavigator = createStackNavigator({
    Screen1: { screen: Screen1 },
    Screen2: { screen: Screen2 },
    }
);
const AppContainer = createAppContainer(AppNavigator);
function getCurrentRouteName(navigationState) {
  if (!navigationState) {
    return null;
  }
  const route = navigationState.routes[navigationState.index];
  if (route.routes) {
    return getCurrentRouteName(route);
  }
  return route.routeName;
}

export default class App extends React.Component {

  componentDidMount() {
    RNWhatfixModule.initialize('<ent_id>');
    RNWhatfixModule.setScreenId('Screen1');
  }
  render() {
    return (
      <AppContainer
        onNavigationStateChange={(prevState, currentState, action) => {
          const currentRouteName = getCurrentRouteName(currentState);
          const previousRouteName = getCurrentRouteName(prevState);

          if (previousRouteName !== currentRouteName) {
            RNWhatfixModule.setScreenId(currentRouteName);
          }
        }}
      />
    );
  }
}
```
