/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './src/App';

AppRegistry.registerComponent("MyApp4", () => App);
