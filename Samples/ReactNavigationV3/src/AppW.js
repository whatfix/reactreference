import React, {useEffect} from 'react';
import { createStackNavigator, createAppContainer } from "react-navigation";
import RNWhatfixModule from 'rn-whatfix';

import MTabNavigator from './navigators/TabNavigator';

import MDrawerNavigator from './navigators/DrawerNavigator';

const AppNavigator = createStackNavigator({
        Tab: {
            screen: MTabNavigator,
        },
        Drawer: {
            screen: MDrawerNavigator,
        },
    },
);
const AppContainer = createAppContainer(AppNavigator);
function getCurrentRouteName(navigationState) {
  if (!navigationState) {
    return null;
  }
  const route = navigationState.routes[navigationState.index];
  if (route.routes) {
    return getCurrentRouteName(route);
  }
  return route.routeName;
}

export default class App extends React.Component {

  componentDidMount() {
    RNWhatfixModule.initialize('');
    RNWhatfixModule.setScreenId('Home');
  }
  render() {
    return (
      <AppContainer
        onNavigationStateChange={(prevState, currentState, action) => {
          const currentRouteName = getCurrentRouteName(currentState);
          const previousRouteName = getCurrentRouteName(prevState);

          if (previousRouteName !== currentRouteName) {
            console.log('Hello : Component - ' + currentRouteName);
            RNWhatfixModule.setScreenId(currentRouteName);
          }
        }}
      />
    );
  }
}
