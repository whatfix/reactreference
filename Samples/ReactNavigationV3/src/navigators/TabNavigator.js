import { createBottomTabNavigator } from 'react-navigation';

import HomeScreen from '../components/HomeScreen';
import AboutScreen from '../components/AboutScreen';

const MTabNavigator = createBottomTabNavigator({
  Home: HomeScreen,
  About: AboutScreen,
});

export default MTabNavigator;
