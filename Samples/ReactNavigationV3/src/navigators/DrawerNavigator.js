import { createDrawerNavigator } from 'react-navigation';

import SettingsScreen from '../components/SettingsScreen';
import ListScreen from '../components/ListScreen';

const MDrawerNavigator = createDrawerNavigator(
    {
      Settings: SettingsScreen,
      List: ListScreen,
    },
    {
      drawerBackgroundColor: 'rgba(255,255,255,.9)',
      contentOptions: {
        activeTintColor: '#fff',
        activeBackgroundColor: '#6b52ae',
      },
    }
);

export default MDrawerNavigator;

