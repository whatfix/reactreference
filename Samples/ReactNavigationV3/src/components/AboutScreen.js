import React, {Component} from 'react';
import {View, Text, Button} from 'react-native';

export default class AboutScreen extends Component {
  render() {
    return (
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <Text>About Screen</Text>
        <Button
          title="Go to Drawer"
          onPress={() => this.props.navigation.navigate('Drawer')}
        />
      </View>
    );
  }
}
