import React, {Component} from 'react';
import {View, Text} from 'react-native';

export default class ListScreen extends Component {
  render() {
    return (
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <Text>List Screen</Text>
      </View>
    );
  }
}
