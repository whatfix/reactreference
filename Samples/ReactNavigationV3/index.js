/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './src/App';

AppRegistry.registerComponent("MyApp3", () => App);
