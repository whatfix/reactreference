import { TabNavigator } from 'react-navigation';

import HomeScreen from '../components/HomeScreen';
import AboutScreen from '../components/AboutScreen';

const MTabNavigator=new TabNavigator({
  Home: { screen: HomeScreen },
  About: { screen: AboutScreen },
});
export default MTabNavigator;
