import { DrawerNavigator } from 'react-navigation';

import SettingsScreen from '../components/SettingsScreen';
import ListScreen from '../components/ListScreen';

const MDrawerNavigator = new DrawerNavigator(
    {
      Settings: SettingsScreen,
      List: ListScreen,
    },
    {
      drawerBackgroundColor: 'rgba(255,255,255,.9)',
      contentOptions: {
        activeTintColor: '#fff',
        activeBackgroundColor: '#6b52ae',
      },
    }
);

export default MDrawerNavigator;

