# Integrate Whatfix with React Native App

Welcome to the official Whatfix React Native SDK. This article explains how to install and update the Whatfix React Native SDK in your project.

## Prerequisites

Node.js Environment

For Android : -
minSdkVersion - 19

For iOS : -
iOS 8.0+, Objective-C / Swift 5.2

### Step 1 - Getting started

To integrate Whatfix react package into your current project root, please run the below command from the terminal.

`$ npm install rn-whatfix --save`

### Step 2 - Initializing Whatfix

Import Whatfix module in your App.js file

`import RNWhatfixModule from 'rn-whatfix';`

Initialize Whatfix in your App.js file

`RNWhatfixModule.initialize(<ent_id>);`

Replace <ent_id> with your Whatfix account ID. To get your ENT ID, email us at support@whatfix.com or get in touch with your Account Manager.

### Segmentation

Set screen for Segmentation using

`RNWhatfixModule.setScreenId(<screenName>);`

## Usage

[React Navigation Version 5.x](https://bitbucket.org/whatfix/reactreference/src/master/Samples/ReactNavigationV5/)

[React Navigation Version 4.x](https://bitbucket.org/whatfix/reactreference/src/master/Samples/ReactNavigationV4/)

[React Navigation Version 3.x](https://bitbucket.org/whatfix/reactreference/src/master/Samples/ReactNavigationV3/)

[React Navigation Version 2.x](https://bitbucket.org/whatfix/reactreference/src/master/Samples/ReactNavigationV2/)

[React Navigation Version 1.x](https://bitbucket.org/whatfix/reactreference/src/master/Samples/ReactNavigationV1/)



